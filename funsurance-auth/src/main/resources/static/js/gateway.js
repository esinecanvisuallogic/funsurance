angular.module('gateway', []).config(function($httpProvider) {

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

}).controller('navigation',

    function($http) {

        var self = this;

        var authenticate = function(credentials, callback) {

            var headers = credentials ? {
                authorization : "Basic "
                + btoa(credentials.username + ":"
                    + credentials.password)
            } : {};

            self.firstTry = true;
            self.user = ''
            $http.get('user', {
                headers : headers
            }).then(function(response) {
                var data = response.data;
                if (data.name) {
                    self.authenticated = true;
                    self.user = data.name
                    self.admin = data && data.roles && data.roles.indexOf("ROLE_ADMIN")>-1;
                    self.registerError = false;
                    self.registerSuccess = false;
                } else {
                    self.authenticated = false;
                    self.admin = false;
                }
                callback && callback(true);
            }, function() {
                if(self.credentials.username.length > 0){
                    self.error = true;
                }
                self.errMsg = "Wrong username or password!";
                self.authenticated = false;
                callback && callback(false);
            });

        };

        authenticate();

        self.loginOrRegister = function () {
            self.registerSuccess = false;
            self.registerError = false;
            self.error = false;
            if(self.registerMode){
                self.register();
            }else{
                self.login();
            }
        };

        self.credentials = {};
        self.login = function() {
            authenticate(self.credentials, function(authenticated) {
                $http.get('app/', self.credentials).then(function(response) {
                    self.result = response;
                })
                self.authenticated = authenticated;
                self.error = !authenticated;
            }, function() {
                if(self.credentials.username.length > 0){
                    self.error = true;
                }
                self.errMsg = "You're not logged in.";
                self.authenticated = false;
                callback && callback(false);
            });
        };

        self.register = function () {
            $http.post('register', self.credentials).then(function(response) {
                if (response.data){
                    self.registerMode = false;
                    self.registerSuccess = true;
                    self.registerError = false;
                    self.error = false;
                }
            }, function(errorResponse){
                self.error = true;
                self.registerSuccess = false;
                self.errMsg = errorResponse.data.message;
            });
        };

        self.logout = function() {
            $http.post('logout', {}).finally(function() {
                self.authenticated = false;
                self.admin = false;
                self.registerSuccess = false;
                self.registerError = true;
                self.error = false;
            });
        };

    });