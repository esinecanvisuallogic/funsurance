package org.esinecan.funsurance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * Zuul enabled Spring Boot Application to facilitate access to our other microservices.
 */
@SpringBootApplication
@EnableZuulProxy
public class FunsuranceAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(FunsuranceAuthApplication.class, args);
	}
}
