package org.esinecan.funsurance.auth.service.implementation;

import org.esinecan.funsurance.auth.configuration.InitializationPropertiesConfigration;
import org.esinecan.funsurance.auth.service.contract.UserService;
import org.esinecan.funsurance.auth.validation.error.EmailExistsException;
import org.esinecan.funsurance.auth.web.dto.UserDto;
import org.esinecan.funsurance.user.model.Role;
import org.esinecan.funsurance.user.model.User;
import org.esinecan.funsurance.user.repository.RoleRepository;
import org.esinecan.funsurance.user.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by eren.sinecan
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private InitializationPropertiesConfigration initializationPropertiesConfigration;

    @Autowired
    private RoleRepository roleRepository;


    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    @Transactional
    public User registerNewUserAccount(UserDto userDto) throws EmailExistsException {
        if (emailExists(userDto.getUsername())){
            throw new EmailExistsException("There is already an account with that username adress: " +userDto.getUsername());
        }
        else {
            Collection<Role> roles = Collections.singletonList(roleRepository.findByName(
                    "ROLE_" + initializationPropertiesConfigration.getUserRole()
            ));

            User user = new User();
            user.setUsername(userDto.getUsername());
            user.setPassword(passwordEncoder.encode(userDto.getPassword()));
            user.setRoles(roles);
            userRepository.save(user);

            logger.info("A new user has been registered: " + user);

            return user;
        }
    }

    private boolean emailExists(String username) {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            return true;
        }
        return false;
    }
}
