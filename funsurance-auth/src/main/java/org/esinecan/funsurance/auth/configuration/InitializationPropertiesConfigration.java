package org.esinecan.funsurance.auth.configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Reads some initial parameters. Admin fields are from when I was planning an admin application as well,
 * but I did not have the time to.
 * Created by eren.sinecan
 */
@Component
@PropertySource(value = "/initialization.properties", ignoreResourceNotFound = true)
@Getter
public class InitializationPropertiesConfigration {

    @Value("${funsurance.admin.username:admin@funsurance.com}")
    private String adminUsername;

    @Value("${funsurance.admin.password:password}")
    private String adminPassword;

    @Value("${funsurance.privilege.read:READ_PRIVILEGE}")
    private String readPrivilege;

    @Value("${funsurance.privilege.write:WRITE_PRIVILEGE}")
    private String writePrivilege;

    @Value("${funsurance.role.user:USER}")
    private String userRole;

    @Value("${funsurance.role.admin:ADMIN}")
    private String adminRole;
}
