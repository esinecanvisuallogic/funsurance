package org.esinecan.funsurance.auth.validation.validator;

import com.google.common.base.Joiner;
import org.esinecan.funsurance.auth.validation.annotation.ValidPassword;
import org.passay.*;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

/**
 * This Validator checks if the password is between 5-30 characters and has no whitespace.
 * I did not add any other rules for ease of testing.
 * Created by eren.sinecan
 */
public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

    @Override
    public void initialize(final ValidPassword arg0) {

    }

    @Override
    public boolean isValid(final String password, final ConstraintValidatorContext context) {
        //final PasswordValidator validator = new PasswordValidator(Arrays.asList(new LengthRule(5, 30), new UppercaseCharacterRule(1), new DigitCharacterRule(1), new SpecialCharacterRule(1), new WhitespaceRule()));
        final PasswordValidator validator = new PasswordValidator(Arrays.asList(new LengthRule(5, 30), new WhitespaceRule()));
        final RuleResult result = validator.validate(new PasswordData(password));
        if (result.isValid()) {
            return true;
        }
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(Joiner.on("\n").join(validator.getMessages(result))).addConstraintViolation();
        return false;
    }

}