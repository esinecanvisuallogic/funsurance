package org.esinecan.funsurance.auth.service.contract;


import org.esinecan.funsurance.auth.validation.error.EmailExistsException;
import org.esinecan.funsurance.auth.web.dto.UserDto;
import org.esinecan.funsurance.user.model.User;

/**
 * Contract for UserServiceImpl
 * Created by eren.sinecan
 */
public interface UserService {

    User registerNewUserAccount(UserDto accountDto) throws EmailExistsException;
}
