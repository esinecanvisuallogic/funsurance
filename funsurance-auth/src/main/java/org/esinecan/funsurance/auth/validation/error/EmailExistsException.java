package org.esinecan.funsurance.auth.validation.error;

/**
 * Created by eren.sinecan
 */
@SuppressWarnings("serial")
public class EmailExistsException extends Throwable {

    public EmailExistsException(final String message) {
        super(message);
    }

}