package org.esinecan.funsurance.auth.exception;

import org.esinecan.funsurance.auth.validation.error.EmailExistsException;
import org.esinecan.funsurance.exception.BaseExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Custom ControllerAdvice for authorization endpoints.
 * Created by eren.sinecan
 */
@RestControllerAdvice
public class AuthExceptionHandler extends BaseExceptionHandler{
    public AuthExceptionHandler(){
        super();
        registerMapping(UsernameNotFoundException.class, "USER_NOT_FOUND", "User not found", HttpStatus.NOT_FOUND);
        registerMapping(EmailExistsException.class, "USER_ALREADY_EXISTS", "A user with this address already exists.", HttpStatus.CONFLICT);
        registerMapping(BadCredentialsException.class, "USER_NOT_CORRECT", "You have entered an incorrect email or password", HttpStatus.BAD_REQUEST);
    }
}
