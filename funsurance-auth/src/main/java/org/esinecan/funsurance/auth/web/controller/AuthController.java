package org.esinecan.funsurance.auth.web.controller;

import org.esinecan.funsurance.auth.service.contract.UserService;
import org.esinecan.funsurance.auth.validation.error.EmailExistsException;
import org.esinecan.funsurance.auth.web.dto.UserDto;
import org.esinecan.funsurance.user.service.contract.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Standard issue auth endpoints.
 * Created by eren.sinecan
 */
@RestController
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;

    /**
     * This endpoint is to figure out if the user is already authenticated and
     * setting the auth headers. Thus, it includes the up-to-date role info too.
     * @param user
     * @return
     */
    @RequestMapping("/user")
    public Map<String, Object> user(Principal user) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put("name", user.getName());
        map.put("roles", map.put("roles", userRoleService.getUserRolesCSV(user.getName())));
        return map;
    }

    /**
     * Method to trigger Spring Security login mechanism.
     * @return
     */
    @RequestMapping("/login")
    public String login() {
        return "forward:/";
    }


    /**
     * New user endpoint.
     * @param accountDto
     * @return
     * @throws EmailExistsException
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public boolean registerUserAccount(@Valid @RequestBody UserDto accountDto) throws EmailExistsException {
        userService.registerNewUserAccount(accountDto);
        return true;
    }

}
