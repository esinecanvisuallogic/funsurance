package org.esinecan.funsurance.auth.configuration;

import org.esinecan.funsurance.configuration.RoleConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Since we use H2 as the database, data resets every time we restart the app.
 * These are some configurations we need when we start the app.
 * Created by eren.sinecan
 */
@Component
public class InitialDataLoader implements
        ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = false;

    @Autowired
    private InitializationPropertiesConfigration initializationProperties;

    @Autowired
    private RoleConfigurer roleConfigurer;

    /**
     * Checks if the roles and privileges specified in <em>initialization.properties<em/> exist.
     * Creates them otherwise.
     * @param event
     */
    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup)
            return;

        roleConfigurer.configureRolesAndPrivileges(initializationProperties.getAdminRole(),
                initializationProperties.getUserRole(), initializationProperties.getReadPrivilege(),
                initializationProperties.getWritePrivilege());

        alreadySetup = true;
    }


}