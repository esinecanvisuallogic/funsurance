package org.esinecan.funsurance.auth.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.embedded.RedisServer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.sql.SQLException;

/**
 * This class starts and stops our embedded Redis instance along with the app.
 * Created by eren.sinecan
 */
@Component
public class EmbeddedDBStarter {
    @Value("${spring.redis.port}")
    private int redisPort;

    private RedisServer redisServer;

    private static final Logger logger = LoggerFactory.getLogger(EmbeddedDBStarter.class);

    @PostConstruct
    public void start() throws IOException, SQLException {
        logger.debug("Starting redis!!!");
        redisServer = new RedisServer(redisPort);
        try {
            redisServer.start();
        }catch (RuntimeException e){
            logger.error("Failed to start Redis Server. " +
                    "If there isn't an instance already running, application will have errors");
        }
    }

    @PreDestroy
    public void stop() {
        redisServer.stop();
    }
}
