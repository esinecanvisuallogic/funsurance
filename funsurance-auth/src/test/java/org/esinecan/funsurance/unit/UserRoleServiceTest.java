package org.esinecan.funsurance.unit;

import org.esinecan.funsurance.unit.base.BaseServiceTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan
 */
public class UserRoleServiceTest extends BaseServiceTest {

    @Test
    public void testGetUserRolesCSV(){
        String role = userRoleService.getUserRolesCSV(username);
        assertEquals(role, "ROLE_USER");
    }
}
