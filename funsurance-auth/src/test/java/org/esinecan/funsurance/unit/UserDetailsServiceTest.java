package org.esinecan.funsurance.unit;

import org.esinecan.funsurance.unit.base.BaseServiceTest;
import org.junit.Test;
import org.springframework.security.core.userdetails.UserDetails;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan
 */
public class UserDetailsServiceTest extends BaseServiceTest {

    @Test
    public void testUserDetailsService(){
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);

        assertEquals(user.getUsername(), userDetails.getUsername());
        assertEquals(user.getPassword(), userDetails.getPassword());
        assertEquals(userDetails.getAuthorities().size(), 1);
        assertTrue(userDetails.getAuthorities().stream().anyMatch(o -> o.getAuthority().contains("USER")));
        assertTrue(userDetails.isAccountNonExpired());
        assertTrue(userDetails.isAccountNonLocked());
        assertTrue(userDetails.isCredentialsNonExpired());
        assertTrue(userDetails.isEnabled());
    }


}
