package org.esinecan.funsurance.unit.base;

import org.esinecan.funsurance.auth.service.contract.UserService;
import org.esinecan.funsurance.auth.service.implementation.UserDetailsServiceImpl;
import org.esinecan.funsurance.auth.validation.error.EmailExistsException;
import org.esinecan.funsurance.auth.web.dto.UserDto;
import org.esinecan.funsurance.user.model.User;
import org.esinecan.funsurance.user.service.contract.UserRoleService;
import org.esinecan.funsurance.FunsuranceAuthApplication;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by eren.sinecan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource("classpath:initialization.properties")
@SpringBootTest(classes = FunsuranceAuthApplication.class)
public abstract class BaseServiceTest {

    @Autowired
    public UserDetailsServiceImpl userDetailsService;

    @Autowired
    public UserService userService;

    @Autowired
    public UserRoleService userRoleService;

    public User user;
    public String username = "testdetails@test.com";
    public String password = "password";

    @Before
    public void registerFirst(){
        UserDto userDto = new UserDto();
        userDto.setUsername(username);
        userDto.setPassword(password);
        userDto.setMatchingPassword(password);

        try {
            user = userService.registerNewUserAccount(userDto);
        } catch (EmailExistsException e) {
            e.printStackTrace();
        }
    }
}
