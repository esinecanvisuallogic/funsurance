package org.esinecan.funsurance.service.implementation;

import org.esinecan.funsurance.entity.archetype.model.InsuranceType;
import org.esinecan.funsurance.entity.archetype.repository.InsuranceTypeRepository;
import org.esinecan.funsurance.service.contract.TariffCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The service to calculate tariff to both show the user and save.
 * Created by eren.sinecan
 */
@Service
public class TariffCalculationServiceImpl implements TariffCalculationService {

    @Autowired
    private InsuranceTypeRepository insuranceTypeRepository;

    /**
     * Takes in the type code and the coverage, retrieves the type, using that info, calculates the tariff.
     * @param insuranceTypeCode
     * @param coverage
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public Double calculateByTypeId(String insuranceTypeCode, Double coverage) {
        InsuranceType insuranceType = insuranceTypeRepository.findById(insuranceTypeCode);
        if(insuranceType == null){
            throw new IllegalArgumentException("The insurance type selected is not valid");
        }
        return calculateByType(insuranceType, coverage);
    }

    /**
     * Takes in the type and the coverage, using that info, calculates the tariff.
     * @param insuranceType
     * @param coverage
     * @return
     */
    @Override
    public Double calculateByType(InsuranceType insuranceType, Double coverage) {
        Double tariff = 0.0;
        if(insuranceType.getMaximumCoverage() < coverage || insuranceType.getMinimumCoverage() > coverage){
            throw new IllegalArgumentException("The insurance type and coverage do not match");
        }
        tariff = insuranceType.getRiskPercentage() * coverage / 12 / 100;
        return tariff;
    }
}
