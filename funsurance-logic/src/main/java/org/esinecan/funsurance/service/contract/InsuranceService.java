package org.esinecan.funsurance.service.contract;

import org.esinecan.funsurance.entity.archetype.model.InsuranceType;
import org.esinecan.funsurance.entity.base.model.IBaseInsurance;
import org.esinecan.funsurance.logic.dto.BaseInsuranceDTO;
import org.esinecan.funsurance.logic.dto.IInsuranceDTO;

import java.security.Principal;
import java.util.List;

/**
 * Created by eren.sinecan
 */
public interface InsuranceService {

    List<IBaseInsurance> getInsurances(Principal principal);

    IBaseInsurance createInsurancePolicy(Principal principal, IInsuranceDTO iBaseInsuranceDTO) throws IllegalAccessException, InstantiationException, NoSuchFieldException;

    List<IBaseInsurance> deleteInsurance(Principal principal, BaseInsuranceDTO iBaseInsuranceDTO) throws NoSuchFieldException, IllegalAccessException;

    List<InsuranceType> getInsuranceCategories();
}
