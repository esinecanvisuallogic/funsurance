package org.esinecan.funsurance.service.implementation;

import org.esinecan.funsurance.entity.archetype.model.InsuranceType;
import org.esinecan.funsurance.entity.archetype.repository.InsuranceTypeRepository;
import org.esinecan.funsurance.entity.base.model.BaseInsurance;
import org.esinecan.funsurance.entity.base.model.IBaseInsurance;
import org.esinecan.funsurance.logic.dto.BaseInsuranceDTO;
import org.esinecan.funsurance.logic.dto.IInsuranceDTO;
import org.esinecan.funsurance.logic.dto.InsuranceTypeDTO;
import org.esinecan.funsurance.logic.strategy.Strategy;
import org.esinecan.funsurance.logic.strategy.StrategyMapper;
import org.esinecan.funsurance.service.contract.InsuranceService;
import org.esinecan.funsurance.service.contract.TariffCalculationService;
import org.esinecan.funsurance.user.model.User;
import org.esinecan.funsurance.user.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This is the Service that maps the incoming IInsuranceDTO to an insurance entity and feeds it to the appropriate
 * Strategy.
 * Created by eren.sinecan
 */
@Service
public class InsuranceServiceImpl implements InsuranceService{

    @Autowired
    private StrategyMapper mapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private InsuranceTypeRepository insuranceTypeRepository;

    @Autowired
    private TariffCalculationService tariffCalculationService;

    /**
     * Iterates over insurance types and using the auth info on the request received by the controller earlier,
     * it retrieves insurance policies of each type that belong to the current user.
     * @param principal
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<IBaseInsurance> getInsurances(Principal principal) {
        User user = userRepository.findByUsername(principal.getName());
        List<IBaseInsurance> insurances = new ArrayList<>();

        Arrays.stream(Strategy.values()).forEach(strategies ->
            insurances.addAll(mapper.getInsuranceDetails(strategies, user)));

        return insurances;
    }

    /**
     * using Reflection, it sets the type agnostic IInsuranceDTO's properties into a new Insurance object. Then using
     * the type info inside the IInsuranceDTO, deduces the appropriate creation strategy and feeds Insurance into it.
     * Also contains some anti-tampering validation.
     * @param principal
     * @param iBaseInsuranceDTO
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws NoSuchFieldException
     */
    @Override
    @Transactional
    public IBaseInsurance createInsurancePolicy(Principal principal, IInsuranceDTO iBaseInsuranceDTO)
            throws IllegalAccessException, InstantiationException, NoSuchFieldException {

        String insuranceTypeId = ((InsuranceTypeDTO) extractFieldValueFromSuper(iBaseInsuranceDTO, "insuranceType")).getId();

        InsuranceType insuranceType = insuranceTypeRepository.findById(insuranceTypeId);

        Double coverage = (Double) extractFieldValueFromSuper(iBaseInsuranceDTO, "coverage");

        if(insuranceType.getMaximumCoverage() < coverage || insuranceType.getMinimumCoverage() > coverage){
            throw new IllegalArgumentException("Coverage input seems to be tampered with!");
        }

        //We freshly calculate this when saving. Can't trust the user.
        extractFieldFromSuper(iBaseInsuranceDTO, "tariff").set(iBaseInsuranceDTO,
                tariffCalculationService.calculateByType(insuranceType, coverage));

        Strategy strategy = Strategy.getStrategyByValue(insuranceTypeId);

        Class<? extends BaseInsurance> insuranceClass = strategy.getInsuranceClass();
        IBaseInsurance insurance = insuranceClass.newInstance();
        BeanUtils.copyProperties(iBaseInsuranceDTO, insurance);
        extractFieldFromSuper(insurance, "insuranceType").set(insurance, insuranceType);

        User user = userRepository.findByUsername(principal.getName());

        return mapper.saveInsurance(strategy, insurance, user);
    }

    private Object extractFieldValueFromSuper(Object object, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        Field declaredField = extractFieldFromSuper(object, fieldName);
        return declaredField.get(object.getClass().getSuperclass().cast(object));
    }

    private Field extractFieldFromSuper(Object object, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        Class<?> clazz = object.getClass();
        Field declaredField = clazz.getSuperclass().getDeclaredField(fieldName);
        declaredField.setAccessible(true);
        return declaredField;
    }

    /**
     * Similar to createInsurancePolicy, this method takes in the type agnostic BaseInsuranceDTO in. But not a
     * IInsuranceDTO this time, because we don't need the versatility of changing between insurance types. So it just
     * extracts the id and the type info and runs the appropriate deletion strategy.
     * @param principal
     * @param iBaseInsuranceDTO
     * @return
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    @Override
    @Transactional
    public List<IBaseInsurance> deleteInsurance(Principal principal, BaseInsuranceDTO iBaseInsuranceDTO) throws NoSuchFieldException, IllegalAccessException {

        Long insuranceId = (Long) extractFieldValueFromSuper(iBaseInsuranceDTO, "id");

        User user = userRepository.findByUsername(principal.getName());

        String insuranceTypeId = ((InsuranceTypeDTO) extractFieldValueFromSuper(iBaseInsuranceDTO, "insuranceType")).getId();

        Strategy strategy = Strategy.getStrategyByValue(insuranceTypeId);

        IBaseInsurance iBaseInsurance = mapper.getInsuranceDetail(strategy, insuranceId);

        User dbUser = (User) extractFieldValueFromSuper(iBaseInsurance, "user");

        if(!dbUser.getUsername().equals(user.getUsername())){
            throw new IllegalArgumentException(user.getUsername() + " attempted to delete a " +
                    "policy belonging to" + dbUser.getUsername());
        }

        mapper.deleteInsurance(strategy, insuranceId);

        return getInsurances(principal);
    }

    /**
     * Returns all existing Insurance Types
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<InsuranceType> getInsuranceCategories() {
        return insuranceTypeRepository.findAll();
    }
}
