package org.esinecan.funsurance.service.contract;

import org.esinecan.funsurance.entity.archetype.model.InsuranceType;

/**
 * Created by eren.sinecan
 */
public interface TariffCalculationService {
    Double calculateByTypeId(String insuranceTypeCode, Double coverage);

    Double calculateByType(InsuranceType insuranceType, Double coverage);
}
