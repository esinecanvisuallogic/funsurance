package org.esinecan.funsurance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by eren.sinecan
 */
@SpringBootApplication
public class FunsuranceLogicApplication {
	public static void main(String[] args) {
		SpringApplication.run(FunsuranceLogicApplication.class, args);
	}

}