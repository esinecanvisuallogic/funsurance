package org.esinecan.funsurance.logic.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import static org.esinecan.funsurance.logic.strategy.Strategy.StrategyConstants.*;

/**
 * This interface is the Type that most of our controller methods take as the @RequestBody.
 * That's because it contains the jackson polymorphic mapping that enables our controllers to receive any of the
 * existing insurance module types as @RequestBody
 * Created by eren.sinecan
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "name")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BikeInsuranceDTO.class, name = BIKE_CODE),
        @JsonSubTypes.Type(value = ElectronicsInsuranceDTO.class, name = ELECTRONICS_CODE),
        @JsonSubTypes.Type(value = SportsEquipmentInsuranceDTO.class, name = SPORTS_CODE),
        @JsonSubTypes.Type(value = JewelleryInsuranceDTO.class, name = JEWELLERY_CODE),
})
public interface IInsuranceDTO {
}
