package org.esinecan.funsurance.logic.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * DTO counterpart of BaseInsurance
 * Created by eren.sinecan
 */
@Getter
@Setter
@NoArgsConstructor
public class BaseInsuranceDTO implements IInsuranceDTO {

    @NotNull
    private InsuranceTypeDTO insuranceType;

    private Long id;

    private Double coverage;

    private Double tariff;
}
