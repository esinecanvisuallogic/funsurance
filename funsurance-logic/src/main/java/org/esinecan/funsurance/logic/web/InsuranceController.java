package org.esinecan.funsurance.logic.web;

import org.esinecan.funsurance.entity.archetype.model.InsuranceType;
import org.esinecan.funsurance.entity.base.model.IBaseInsurance;
import org.esinecan.funsurance.logic.dto.BaseInsuranceDTO;
import org.esinecan.funsurance.logic.dto.IInsuranceDTO;
import org.esinecan.funsurance.service.contract.InsuranceService;
import org.esinecan.funsurance.service.contract.TariffCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Created by eren.sinecan
 */
@RestController
public class InsuranceController {

    @Autowired
    private InsuranceService insuranceService;

    @Autowired
    private TariffCalculationService tariffCalculationService;


    @RequestMapping(value = "/client_insurances")
    public List<IBaseInsurance> getInsurances(Principal principal){
        return insuranceService.getInsurances(principal);
    }


    @RequestMapping(value = "/create_insurance")
    public IBaseInsurance addInsurance(Principal principal, @RequestBody @Valid IInsuranceDTO iBaseInsuranceDTO)
            throws InstantiationException, IllegalAccessException, NoSuchFieldException {
        return insuranceService.createInsurancePolicy(principal, iBaseInsuranceDTO);
    }

    @RequestMapping(value = "/delete_insurance")
    public List<IBaseInsurance> deleteInsurance(Principal principal,
                                                @RequestBody @Valid BaseInsuranceDTO iBaseInsuranceDTO) throws NoSuchFieldException, IllegalAccessException {
        return insuranceService.deleteInsurance(principal, iBaseInsuranceDTO);
    }

    @RequestMapping(value = "/insurance_types")
    public List<InsuranceType> getInsuranceCategories(){
        return insuranceService.getInsuranceCategories();
    }

    @RequestMapping(value = "/calculate_coverage")
    public Double calculateTariff(@RequestBody BaseInsuranceDTO insuranceDTO){
        return tariffCalculationService.calculateByTypeId(insuranceDTO.getInsuranceType().getId(), insuranceDTO.getCoverage());
    }

}
