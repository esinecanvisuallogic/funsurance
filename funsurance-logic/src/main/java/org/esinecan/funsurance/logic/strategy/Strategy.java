package org.esinecan.funsurance.logic.strategy;

import org.esinecan.funsurance.entity.base.model.BaseInsurance;
import org.esinecan.funsurance.entity.bike.model.BikeInsurance;
import org.esinecan.funsurance.entity.electronics.model.ElectronicsInsurance;
import org.esinecan.funsurance.entity.jewellery.model.JewelleryInsurance;
import org.esinecan.funsurance.entity.sportsequipment.model.SportsEquipmentInsurance;
import org.esinecan.funsurance.logic.strategy.implementation.BikeStrategyService;
import org.esinecan.funsurance.logic.strategy.implementation.ElectronicsStrategyService;
import org.esinecan.funsurance.logic.strategy.implementation.JewelleryStrategyService;
import org.esinecan.funsurance.logic.strategy.implementation.SportsEquipmentStrategyService;

import java.util.Arrays;

import static org.esinecan.funsurance.logic.strategy.Strategy.StrategyConstants.*;

/**
 * Enum to help our strategy implementations. Each new insurance module is registered here so that we can easiy know
 * what is its entity, and Strategy Service class.
 * Created by eren.sinecan
 */
public enum Strategy {
    BIKE(BIKE_CODE, BikeStrategyService.class, BikeInsurance.class),
    JEWELLERY(JEWELLERY_CODE, JewelleryStrategyService.class, JewelleryInsurance.class),
    ELECTRONICS(ELECTRONICS_CODE,ElectronicsStrategyService.class, ElectronicsInsurance.class),
    SPORTSEQUIPMENT(SPORTS_CODE,SportsEquipmentStrategyService.class, SportsEquipmentInsurance.class);

    private String value;

    private Class<?> serviceClass;
    private Class<? extends BaseInsurance> insuranceClass;

    /**
     * We have to enforce uniqueness of values.
     * Otherwise it might turn into a mess
     */
    static {
        if(Arrays.stream(Strategy.values())
                .map(e -> e.value)
                .distinct()
                .count() < Strategy.values().length){
            throw new IllegalStateException("Please keep insurance module" +
                    " values unique");
        }
    }

    private Strategy(String value, Class<?> serviceClass,
                     Class<? extends BaseInsurance> insuranceClass){
        this.value = value;
        this.serviceClass = serviceClass;
        this.insuranceClass = insuranceClass;
    }

    public static Strategy getStrategyByValue(String value){
        return Arrays.stream(Strategy.values())
                .filter(e -> e.getValue().equals(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unsupported type %s.", value)));
    }

    public String getValue(){
        return value;
    }

    public Class<?> getServiceClass(){
        return serviceClass;
    }

    public Class<? extends BaseInsurance> getInsuranceClass(){
        return insuranceClass;
    }

    public static class StrategyConstants {
        public static final String BIKE_CODE = "B";
        public static final String ELECTRONICS_CODE = "E";
        public static final String JEWELLERY_CODE = "J";
        public static final String SPORTS_CODE = "S";
    }
}
