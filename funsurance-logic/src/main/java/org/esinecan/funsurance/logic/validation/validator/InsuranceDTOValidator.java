package org.esinecan.funsurance.logic.validation.validator;

import org.esinecan.funsurance.entity.archetype.model.InsuranceType;
import org.esinecan.funsurance.entity.archetype.repository.InsuranceTypeRepository;
import org.esinecan.funsurance.logic.dto.BaseInsuranceDTO;
import org.esinecan.funsurance.logic.dto.IInsuranceDTO;
import org.esinecan.funsurance.logic.dto.InsuranceTypeDTO;
import org.esinecan.funsurance.logic.validation.annotation.ValidInsuranceDTO;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validation against possible tampering of insuranceTypeDTO.
 * Created by eren.sinecan
 */
public class InsuranceDTOValidator implements ConstraintValidator<ValidInsuranceDTO, IInsuranceDTO> {

    @Autowired
    private InsuranceTypeRepository insuranceTypeRepository;

    @Override
    public void initialize(ValidInsuranceDTO constraintAnnotation) {
    }

    @Override
    public boolean isValid(IInsuranceDTO value, ConstraintValidatorContext context) {
        BaseInsuranceDTO baseInsuranceDTO = (BaseInsuranceDTO) value;
        InsuranceTypeDTO dto = baseInsuranceDTO.getInsuranceType();
        InsuranceType type = insuranceTypeRepository.findById(dto.getId());

        //Dramatic variable naming
        boolean isInsureTypeUnsullied = (type.getId() != dto.getId()) || (type == null) || (dto == null) ||
                (type.getMinimumCoverage() != dto.getMinimumCoverage()) || (type.getName() != dto.getName()) ||
                (type.getMaximumCoverage() != dto.getMaximumCoverage()) ||
                (type.getRiskPercentage() != dto.getRiskPercentage());

        return isInsureTypeUnsullied;
    }
}
