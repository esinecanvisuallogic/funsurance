package org.esinecan.funsurance.logic.strategy.implementation;

import org.esinecan.funsurance.entity.base.model.IBaseInsurance;
import org.esinecan.funsurance.entity.sportsequipment.model.SportsEquipmentInsurance;
import org.esinecan.funsurance.entity.sportsequipment.repository.SportsEquipmentInsuranceRepository;
import org.esinecan.funsurance.logic.strategy.ILookupInsuranceService;
import org.esinecan.funsurance.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * The specific strategy for Sports Equipment Insurances.
 * Created by eren.sinecan
 */
@Service
public class SportsEquipmentStrategyService implements ILookupInsuranceService{

    @Autowired
    private SportsEquipmentInsuranceRepository sportsEquipmentInsuranceRepository;

    @Override
    @Transactional(readOnly = true)
    public List<IBaseInsurance> findByUser(User user){
        List<IBaseInsurance> insurances = new ArrayList<>();
        List<SportsEquipmentInsurance> sportsEquipmentInsurances = sportsEquipmentInsuranceRepository.findByUser(user);
        sportsEquipmentInsurances.stream().forEach(sportsEquipmentInsurance -> insurances.add(sportsEquipmentInsurance));
        return insurances;
    }

    @Override
    @Transactional(readOnly = true)
    public IBaseInsurance findOne(Long id) {
        return sportsEquipmentInsuranceRepository.findOne(id);
    }

    @Override
    @Transactional
    public IBaseInsurance save(IBaseInsurance insurance, User user){
        SportsEquipmentInsurance sportsEquipmentInsurance = (SportsEquipmentInsurance) insurance;
        sportsEquipmentInsurance.setUser(user);
        return sportsEquipmentInsuranceRepository.save(sportsEquipmentInsurance);
    }

    @Override
    @Transactional
    public void delete(Long insuranceId){
        sportsEquipmentInsuranceRepository.delete(insuranceId);
    }
}
