package org.esinecan.funsurance.logic.strategy.implementation;

import org.esinecan.funsurance.entity.base.model.IBaseInsurance;
import org.esinecan.funsurance.entity.bike.model.BikeInsurance;
import org.esinecan.funsurance.entity.bike.repository.BikeInsuranceRepository;
import org.esinecan.funsurance.logic.strategy.ILookupInsuranceService;
import org.esinecan.funsurance.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * The specific strategy for Bike Insurances.
 * Created by eren.sinecan
 */
@Service
public class BikeStrategyService implements ILookupInsuranceService{

    @Autowired
    private BikeInsuranceRepository bikeInsuranceRepository;

    @Override
    @Transactional(readOnly = true)
    public List<IBaseInsurance> findByUser(User user){
        List<IBaseInsurance> insurances = new ArrayList<>();
        List<BikeInsurance> bikeInsurances = bikeInsuranceRepository.findByUser(user);
        bikeInsurances.stream().forEach(bikeInsurance -> insurances.add(bikeInsurance));
        return insurances;
    }

    @Override
    @Transactional(readOnly = true)
    public IBaseInsurance findOne(Long id) {
        return bikeInsuranceRepository.findOne(id);
    }

    @Override
    @Transactional
    public IBaseInsurance save(IBaseInsurance insurance, User user){
        BikeInsurance bikeInsurance = (BikeInsurance) insurance;
        bikeInsurance.setUser(user);

        return bikeInsuranceRepository.save(bikeInsurance);
    }

    @Override
    @Transactional
    public void delete(Long insuranceId){
        bikeInsuranceRepository.delete(insuranceId);
    }
}
