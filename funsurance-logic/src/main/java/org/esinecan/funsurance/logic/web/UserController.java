package org.esinecan.funsurance.logic.web;

import org.esinecan.funsurance.user.service.contract.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by eren.sinecan
 */
@RestController
public class UserController {

    @Autowired
    UserRoleService userRoleService;

    @RequestMapping("/user")
    public Map<String, String> user(Principal user) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("name", user.getName());
        map.put("roles", userRoleService.getUserRolesCSV(user.getName()));
        return map;
    }
}
