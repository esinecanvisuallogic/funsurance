package org.esinecan.funsurance.logic.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * DTO counterpart of InsuranceType
 * Created by eren.sinecan
 */
@Getter
@Setter
@NoArgsConstructor
public class InsuranceTypeDTO{

    @NotNull
    private String id;

    private Double riskPercentage;

    private Double minimumCoverage;

    private Double maximumCoverage;

    private String name;
}
