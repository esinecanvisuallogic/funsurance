package org.esinecan.funsurance.logic.strategy;

import org.esinecan.funsurance.entity.base.model.IBaseInsurance;
import org.esinecan.funsurance.logic.strategy.implementation.BikeStrategyService;
import org.esinecan.funsurance.logic.strategy.implementation.ElectronicsStrategyService;
import org.esinecan.funsurance.logic.strategy.implementation.JewelleryStrategyService;
import org.esinecan.funsurance.logic.strategy.implementation.SportsEquipmentStrategyService;
import org.esinecan.funsurance.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This Mapper provides us with a convenient way of accessing Strategy Service instances that are already injected into
 * the context.
 * Created by eren.sinecan
 */
@Component
public class StrategyMapper {
    @Autowired
    private BikeStrategyService bikeStrategyService;

    @Autowired
    private ElectronicsStrategyService electronicsStrategyService;

    @Autowired
    private JewelleryStrategyService jewelleryStrategyService;

    @Autowired
    private SportsEquipmentStrategyService sportsEquipmentStrategyService;

    private List<ILookupInsuranceService> insuranceServices = new ArrayList<>();

    @PostConstruct
    private void addInsuranceServices(){
        insuranceServices.add(bikeStrategyService);
        insuranceServices.add(electronicsStrategyService);
        insuranceServices.add(jewelleryStrategyService);
        insuranceServices.add(sportsEquipmentStrategyService);
    }

    private ILookupInsuranceService getInsuranceService(Strategy strategy){

        Optional<ILookupInsuranceService> insuranceService =
                insuranceServices.stream()
                        .filter(iLookupInsuranceService -> {
                            String s = iLookupInsuranceService.getClass().getName();
                            if(s.contains("$$")){
                                return s.split("\\$\\$")[0].equals(strategy.getServiceClass().getName());
                            }
                            return s.equals(strategy.getServiceClass().getName());}).findFirst();

        if(!insuranceService.isPresent()){
            throw new IllegalArgumentException("Can't find the service for: " + strategy.name());
        }

        return insuranceService.get();
    }

    public List<IBaseInsurance> getInsuranceDetails(Strategy strategy, User user){
        ILookupInsuranceService insuranceService = getInsuranceService(strategy);
        return insuranceService.findByUser(user);
    }

    public IBaseInsurance getInsuranceDetail(Strategy strategy, Long id){
        ILookupInsuranceService insuranceService = getInsuranceService(strategy);
        return insuranceService.findOne(id);
    }

    public IBaseInsurance saveInsurance(Strategy strategy, IBaseInsurance insurance, User user){
        ILookupInsuranceService insuranceService = getInsuranceService(strategy);
        return insuranceService.save(insurance, user);
    }

    public void deleteInsurance(Strategy strategy, Long insuranceId){
        ILookupInsuranceService insuranceService = getInsuranceService(strategy);
        insuranceService.delete(insuranceId);
    }
}
