package org.esinecan.funsurance.logic.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;

import static org.esinecan.funsurance.logic.strategy.Strategy.StrategyConstants.ELECTRONICS_CODE;

/**
 * DTO counterpart of ElectronicsInsurance
 * Created by eren.sinecan
 */
@Getter
@Setter
@JsonTypeName(ELECTRONICS_CODE)
public class ElectronicsInsuranceDTO extends BaseInsuranceDTO {

    public ElectronicsInsuranceDTO(){
        super();
    }

    private Double kwPerHour;
}
