package org.esinecan.funsurance.logic.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;

import static org.esinecan.funsurance.logic.strategy.Strategy.StrategyConstants.SPORTS_CODE;

/**
 * DTO counterpart of SportsEquipmentInsurance
 * Created by eren.sinecan
 */
@Getter
@Setter
@JsonTypeName(SPORTS_CODE)
public class SportsEquipmentInsuranceDTO extends BaseInsuranceDTO {

    public SportsEquipmentInsuranceDTO(){
        super();
    }

    private String antiquityNotes;
}
