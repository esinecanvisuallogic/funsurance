package org.esinecan.funsurance.logic.config;

import org.esinecan.funsurance.configuration.RoleConfigurer;
import org.esinecan.funsurance.entity.archetype.model.InsuranceType;
import org.esinecan.funsurance.entity.archetype.repository.InsuranceTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Creates the insurance categories specified in specs on application boot time.
 * Created by eren.sinecan
 */
@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = false;

    private static final Logger logger = LoggerFactory.getLogger(InitialDataLoader.class);

    @Autowired
    private AuthorizationConfiguration authorizationConfiguration;

    @Autowired
    private RoleConfigurer roleConfigurer;

    @Autowired
    private InsuranceTypeRepository insuranceTypeRepository;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (alreadySetup)
            return;
        roleConfigurer.configureRolesAndPrivileges(authorizationConfiguration.getAdminRole(),
                authorizationConfiguration.getUserRole(), authorizationConfiguration.getReadPrivilege(),
                authorizationConfiguration.getWritePrivilege());

        InsuranceType bike = new InsuranceType();
        bike.setName("Bike Insurance");
        bike.setMinimumCoverage(0.0);
        bike.setMaximumCoverage(3000.0);
        bike.setRiskPercentage(30.0);
        bike.setId("B");
        insuranceTypeRepository.save(bike);

        InsuranceType jewellery = new InsuranceType();
        jewellery.setName("Jewellery Insurance");
        jewellery.setMinimumCoverage(500.0);
        jewellery.setMaximumCoverage(10000.0);
        jewellery.setRiskPercentage(5.0);
        jewellery.setId("J");
        insuranceTypeRepository.save(jewellery);

        InsuranceType electronics = new InsuranceType();
        electronics.setName("Electronics Insurance");
        electronics.setMinimumCoverage(500.0);
        electronics.setMaximumCoverage(6000.0);
        electronics.setRiskPercentage(35.0);
        electronics.setId("E");
        insuranceTypeRepository.save(electronics);

        InsuranceType sportsEquipment = new InsuranceType();
        sportsEquipment.setName("Sports Equipment Insurance");
        sportsEquipment.setMinimumCoverage(0.0);
        sportsEquipment.setMaximumCoverage(20000.0);
        sportsEquipment.setRiskPercentage(30.0);
        sportsEquipment.setId("S");
        insuranceTypeRepository.save(sportsEquipment);

        alreadySetup = true;
    }
}
