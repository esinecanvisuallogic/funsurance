package org.esinecan.funsurance.logic.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;

import static org.esinecan.funsurance.logic.strategy.Strategy.StrategyConstants.BIKE_CODE;

/**
 * Created by eren.sinecan
 */
@Getter
@Setter
@JsonTypeName(BIKE_CODE)
public class BikeInsuranceDTO extends BaseInsuranceDTO {

    public BikeInsuranceDTO(){
        super();
    }

    private String serialNumber;
}
