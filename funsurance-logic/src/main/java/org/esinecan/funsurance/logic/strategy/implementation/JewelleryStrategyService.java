package org.esinecan.funsurance.logic.strategy.implementation;

import org.esinecan.funsurance.entity.base.model.IBaseInsurance;
import org.esinecan.funsurance.entity.jewellery.model.JewelleryInsurance;
import org.esinecan.funsurance.entity.jewellery.repository.JewelleryInsuranceRepository;
import org.esinecan.funsurance.logic.strategy.ILookupInsuranceService;
import org.esinecan.funsurance.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * The specific strategy for Jewellery Insurances.
 * Created by eren.sinecan
 */
@Service
public class JewelleryStrategyService implements ILookupInsuranceService{
    @Autowired
    private JewelleryInsuranceRepository jewelleryInsuranceRepository;

    @Override
    @Transactional(readOnly = true)
    public List<IBaseInsurance> findByUser(User user){
        List<IBaseInsurance> insurances = new ArrayList<>();
        List<JewelleryInsurance> jewelleryInsurances = jewelleryInsuranceRepository.findByUser(user);
        jewelleryInsurances.stream().forEach(jewelleryInsurance -> insurances.add(jewelleryInsurance));
        return insurances;
    }

    @Override
    @Transactional(readOnly = true)
    public IBaseInsurance findOne(Long id) {
        return jewelleryInsuranceRepository.findOne(id);
    }

    @Override
    @Transactional
    public IBaseInsurance save(IBaseInsurance insurance, User user){
        JewelleryInsurance jewelleryInsurance = (JewelleryInsurance) insurance;
        jewelleryInsurance.setUser(user);

        return jewelleryInsuranceRepository.save(jewelleryInsurance);
    }

    @Override
    @Transactional
    public void delete(Long insuranceId){
        jewelleryInsuranceRepository.delete(insuranceId);
    }
}
