package org.esinecan.funsurance.logic.validation.exception;

import org.esinecan.funsurance.exception.BaseExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Custom ControllerAdvice for authorization endpoints.
 * Created by eren.sinecan
 */
@RestControllerAdvice
public class LogicExceptionHandler extends BaseExceptionHandler {
    public LogicExceptionHandler(){
        super();
        registerMapping(IllegalStateException.class, "WRONG_ENUM_CONFIGURATION", "Please keep insurance module values unique", HttpStatus.SERVICE_UNAVAILABLE);
        registerMapping(IllegalArgumentException.class, "TAMPERED_WITH", "Dto has some unusual values. It was probably tampered with", HttpStatus.CONFLICT);
        registerMapping(BadCredentialsException.class, "USER_NOT_CORRECT", "You have entered an incorrect email or password", HttpStatus.BAD_REQUEST);
    }
}