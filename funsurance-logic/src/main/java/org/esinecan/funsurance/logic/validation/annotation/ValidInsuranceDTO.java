package org.esinecan.funsurance.logic.validation.annotation;

import org.esinecan.funsurance.logic.validation.validator.InsuranceDTOValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by eren.sinecan
 */
@Documented
@Constraint(validatedBy = InsuranceDTOValidator.class)
@Target({ TYPE, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
public @interface ValidInsuranceDTO {

    String message() default "Object seems to be tampered with";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
