package org.esinecan.funsurance.logic.strategy;


import org.esinecan.funsurance.entity.base.model.IBaseInsurance;
import org.esinecan.funsurance.user.model.User;

import java.util.List;

/**
 * Since we decided that each insurance module type has some unique business logic, (i.e. a primitive field) we can't
 * keep them in a single entity. Therefore we need to differentiate their handling strategies. But we do not want to
 * fill the place with conditional statements, instead we abuse this interface in our "reflection-hack-anti-pattern" as
 * it is  implemented by all our other insurance strategy services.
 * Created by eren.sinecan
 */
public interface ILookupInsuranceService {

    List<IBaseInsurance> findByUser(User user);

    IBaseInsurance findOne(Long id);

    IBaseInsurance save(IBaseInsurance iBaseInsurance, User user);

    void delete(Long insuranceId);
}
