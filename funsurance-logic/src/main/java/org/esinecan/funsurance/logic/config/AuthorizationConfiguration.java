package org.esinecan.funsurance.logic.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * The usual configuration component
 * Created by eren.sinecan
 */
@Component
@PropertySource("/initialization.properties")
@Getter
public class AuthorizationConfiguration {

    @Value("${funsurance.privilege.read}")
    private String readPrivilege;

    @Value("${funsurance.privilege.write}")
    private String writePrivilege;

    @Value("${funsurance.role.user}")
    private String userRole;

    @Value("${funsurance.role.admin}")
    private String adminRole;
}
