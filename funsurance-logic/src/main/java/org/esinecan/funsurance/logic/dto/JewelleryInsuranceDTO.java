package org.esinecan.funsurance.logic.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;

import static org.esinecan.funsurance.logic.strategy.Strategy.StrategyConstants.JEWELLERY_CODE;

/**
 * DTO counterpart of JewelleryInsurance
 * Created by eren.sinecan
 */
@Getter
@Setter
@JsonTypeName(JEWELLERY_CODE)
public class JewelleryInsuranceDTO extends BaseInsuranceDTO {

    public JewelleryInsuranceDTO(){
        super();
    }

    private String carat;
}
