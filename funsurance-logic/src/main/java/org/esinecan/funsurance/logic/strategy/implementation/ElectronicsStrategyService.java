package org.esinecan.funsurance.logic.strategy.implementation;

import org.esinecan.funsurance.entity.base.model.IBaseInsurance;
import org.esinecan.funsurance.entity.electronics.model.ElectronicsInsurance;
import org.esinecan.funsurance.entity.electronics.repository.ElectronicsInsuranceRepository;
import org.esinecan.funsurance.logic.strategy.ILookupInsuranceService;
import org.esinecan.funsurance.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * The specific strategy for Electronics Insurances.
 * Created by eren.sinecan
 */
@Service
public class ElectronicsStrategyService implements ILookupInsuranceService{

    @Autowired
    private ElectronicsInsuranceRepository electronicsInsuranceRepository;

    @Override
    @Transactional(readOnly = true)
    public List<IBaseInsurance> findByUser(User user){
        List<IBaseInsurance> insurances = new ArrayList<>();
        List<ElectronicsInsurance> electronicsInsurances = electronicsInsuranceRepository.findByUser(user);
        electronicsInsurances.stream().forEach(electronicsInsurance -> insurances.add(electronicsInsurance));
        return insurances;
    }

    @Override
    @Transactional(readOnly = true)
    public IBaseInsurance findOne(Long id) {
        return electronicsInsuranceRepository.findOne(id);
    }

    @Override
    @Transactional
    public IBaseInsurance save(IBaseInsurance insurance, User user){
        ElectronicsInsurance electronicsInsurance = (ElectronicsInsurance) insurance;
        electronicsInsurance.setUser(user);

        return electronicsInsuranceRepository.save(electronicsInsurance);
    }

    @Override
    @Transactional
    public void delete(Long insuranceId){
        electronicsInsuranceRepository.delete(insuranceId);
    }
}
