package org.esinecan.funsurance.stories.strategy;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.esinecan.funsurance.FunsuranceLogicApplication;
import org.esinecan.funsurance.entity.sportsequipment.model.SportsEquipmentInsurance;
import org.esinecan.funsurance.entity.sportsequipment.repository.SportsEquipmentInsuranceRepository;
import org.esinecan.funsurance.logic.strategy.Strategy;
import org.esinecan.funsurance.logic.strategy.StrategyMapper;
import org.esinecan.funsurance.user.model.User;
import org.esinecan.funsurance.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan
 */
@ContextConfiguration(
        loader = SpringBootContextLoader.class,
        classes = FunsuranceLogicApplication.class
)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DeleteInsuranceSteps {

    @Autowired
    private UserRepository userRepository;

    private final String username = "test2@test.com";

    private final String password = "password";

    private User user;

    private SportsEquipmentInsurance insurance;

    @Autowired
    SportsEquipmentInsuranceRepository repository;

    @Autowired
    private StrategyMapper strategyMapper;

    @Given("^a username, 'test(\\d+)@test\\.com', is an existing 'User'$")
    public void a_username_test_test_com_is_an_existing_User(int arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        User user = new User();
        user.setPassword(password);
        user.setUsername(username);

        this.user = userRepository.save(user);
    }

    @Given("^'test(\\d+)@test\\.com' insured their 'sportsEquipment'$")
    public void test_test_com_insured_their_sportsEquipment(int arg1) throws Throwable {
        SportsEquipmentInsurance insurance = new SportsEquipmentInsurance();
        insurance.setUser(user);
        insurance.setTariff(23.0);
        insurance.setAntiquityNotes("An antique kettlebell");
        insurance.setCoverage(234.0);

        this.insurance = (SportsEquipmentInsurance) strategyMapper.saveInsurance(Strategy.SPORTSEQUIPMENT, insurance, user);
    }

    @When("^'deleteInsurance' service is called$")
    public void deleteinsurance_service_is_called() throws Throwable {
        strategyMapper.deleteInsurance(Strategy.SPORTSEQUIPMENT, insurance.getId());
    }

    @Then("^An insurance is deleted$")
    public void an_insurance_is_deleted() throws Throwable {
        SportsEquipmentInsurance sportsEquipmentInsurance = repository.findOne(insurance.getId());

        assertEquals(sportsEquipmentInsurance, null);
    }
}
