package org.esinecan.funsurance.stories.strategy;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.esinecan.funsurance.FunsuranceLogicApplication;
import org.esinecan.funsurance.entity.bike.model.BikeInsurance;
import org.esinecan.funsurance.entity.bike.repository.BikeInsuranceRepository;
import org.esinecan.funsurance.logic.strategy.Strategy;
import org.esinecan.funsurance.logic.strategy.StrategyMapper;
import org.esinecan.funsurance.user.model.User;
import org.esinecan.funsurance.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan
 */
@ContextConfiguration(
        loader = SpringBootContextLoader.class,
        classes = FunsuranceLogicApplication.class
)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ListMyInsurancesSteps {

    private final String username = "test@test.com";

    private final String password = "password";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BikeInsuranceRepository bikeInsuranceRepository;

    @Autowired
    private StrategyMapper strategyMapper;

    private User user;

    private BikeInsurance bikeInsurance;


    @Given("^a username, 'test@test\\.com', is an existing 'User'$")
    public void a_username_test_test_com_is_an_existing_User() throws Throwable {
        User user = new User();
        user.setPassword(password);
        user.setUsername(username);

        this.user = userRepository.save(user);
    }

    @Given("^'test@test\\.com' insured their bike$")
    public void test_test_com_insured_their_bike() throws Throwable {
        BikeInsurance bikeInsurance = new BikeInsurance();
        bikeInsurance.setUser(user);
        bikeInsurance.setCoverage(2300.0);
        bikeInsurance.setTariff(12.0);
        bikeInsurance.setSerialNumber("dfdf");

        bikeInsuranceRepository.save(bikeInsurance);
    }

    @When("^'monthlyPremiums' service is called$")
    public void monthlypremiums_service_is_called() throws Throwable {
        bikeInsurance = (BikeInsurance)(strategyMapper.getInsuranceDetails(Strategy.BIKE, user)).get(0);
    }
    @Then("^I see my monthly payment$")
    public void i_see_my_monthly_payment() throws Throwable {
        assertEquals(new Double(12.0), bikeInsurance.getTariff());
    }

    @Then("^I see my coverage$")
    public void i_see_my_coverage() throws Throwable {
        assertEquals(new Double(2300.0), bikeInsurance.getCoverage());
    }

    @Then("^I see my bike's serial number$")
    public void i_see_my_bike_s_serial_number() throws Throwable {
        String serial = "dfdf";
        assertEquals(serial, bikeInsurance.getSerialNumber());
    }
}
