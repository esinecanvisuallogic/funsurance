package org.esinecan.funsurance.stories.strategy;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.esinecan.funsurance.FunsuranceLogicApplication;
import org.esinecan.funsurance.entity.bike.model.BikeInsurance;
import org.esinecan.funsurance.logic.strategy.Strategy;
import org.esinecan.funsurance.logic.strategy.StrategyMapper;
import org.esinecan.funsurance.user.model.User;
import org.esinecan.funsurance.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan
 */
@ContextConfiguration(
        loader = SpringBootContextLoader.class,
        classes = FunsuranceLogicApplication.class
)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AddNewInsuranceSteps {

    @Autowired
    private UserRepository userRepository;

    private final String username = "test1@test.com";

    private final String password = "password";

    private User user;

    private BikeInsurance bikeInsurance;

    @Autowired
    private StrategyMapper strategyMapper;

    @Given("^a username, 'test1@test\\.com', is an existing 'User'$")
    public void a_username_test1_test_com_is_an_existing_User() throws Throwable {
        User user = new User();
        user.setPassword(password);
        user.setUsername(username);

        this.user = userRepository.save(user);
    }

    @When("^'newInsurance' service is called$")
    public void newinsurance_service_is_called() throws Throwable {
        BikeInsurance bikeInsurance = new BikeInsurance();
        bikeInsurance.setUser(user);
        bikeInsurance.setCoverage(2300.0);
        bikeInsurance.setTariff(12.0);
        bikeInsurance.setSerialNumber("dfdf");

        bikeInsurance = (BikeInsurance) strategyMapper.saveInsurance(Strategy.BIKE, bikeInsurance, user);
    }

    @Then("^A new insurance is saved$")
    public void a_new_insurance_is_saved() throws Throwable {
        String serial = "dfdf";
        assertEquals(serial, bikeInsurance.getSerialNumber());
    }
}
