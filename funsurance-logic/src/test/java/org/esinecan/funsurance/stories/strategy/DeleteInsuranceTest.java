package org.esinecan.funsurance.stories.strategy;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.esinecan.funsurance.FunsuranceLogicTests;
import org.junit.runner.RunWith;

/**
 * Created by eren.sinecan
 */
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/org/esinecan/funsurance/stories/strategy/DeleteInsurance.feature")
public class DeleteInsuranceTest extends FunsuranceLogicTests {
}
