package org.esinecan.funsurance.stories.strategy;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.esinecan.funsurance.FunsuranceLogicApplication;
import org.esinecan.funsurance.service.contract.TariffCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan
 */
@ContextConfiguration(
        loader = SpringBootContextLoader.class,
        classes = FunsuranceLogicApplication.class
)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CalculateTariffSteps {

    private String insuranceTypeCode;
    private Double coverage;
    private Double tariff;

    @Autowired
    private TariffCalculationService tariffCalculationService;

    @Given("^We have a InsuranceType and a Coverage$")
    public void we_have_a_InsuranceType_and_a_Coverage() throws Throwable {
        insuranceTypeCode = "B";
        coverage = 3000.0;
    }

    @When("^we enquire tariff service a coverage and Insurance Type$")
    public void we_enquire_tariff_service_a_coverage_and_Insurance_Type() throws Throwable {
        tariff = tariffCalculationService.calculateByTypeId(insuranceTypeCode, coverage);
    }

    @Then("^Type's risk times Coverage is returned\\.$")
    public void type_s_risk_times_Coverage_is_returned() throws Throwable {
        Double expectedTariff = coverage * 0.3 / 12;
        assertEquals(tariff, expectedTariff);
    }

}
