Feature: Tariff Calculation Service

Narrative:
While showing the user and
  while saving, we need a
  service to calculate the
  tariff.

Scenario: New Insurance

Given We have a InsuranceType and a Coverage
When we enquire tariff service a coverage and Insurance Type
Then Type's risk times Coverage is returned.