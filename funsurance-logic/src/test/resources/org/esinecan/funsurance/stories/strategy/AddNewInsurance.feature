Feature: Checking my existing premiums

Narrative:
As a user
I should be able to insure
  my other belongings

Scenario: New Insurance

Given a username, 'testetmeyi@test.com', is an existing 'User'
  And 'testetmeyi@test.com' insured their bike
When 'newInsurance' service is called
Then A new insurance is saved