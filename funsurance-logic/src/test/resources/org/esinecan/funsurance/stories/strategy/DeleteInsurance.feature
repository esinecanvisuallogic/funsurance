Feature: Checking my existing premiums

Narrative:
As a user
I should be able to unsubscribe from my insurances.

Scenario: Delete Insurance

Given a username, 'test2@test.com', is an existing 'User'
  And 'test1@test.com' insured their 'sportsEquipment'
When 'deleteInsurance' service is called
Then An insurance is deleted