Feature: Checking my existing premiums

Narrative:
As a user
I want to look at my modules
So that I can see my monthly payment

Scenario: Checking Premiums
Given a username, 'test@test.com', is an existing 'User'
  And 'test@test.com' insured their bike
When 'monthlyPremiums' service is called
Then I see my monthly payment
  And I see my coverage
  And I see my bike's serial number