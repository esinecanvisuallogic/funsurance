angular.module('clientapp', ['ui.bootstrap']).config(function($httpProvider) {

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

}).controller('home',

    function($http, $rootScope, $uibModal, $log, $timeout, $document) {

        var self = this;

        self.modalParent =
            angular.element($document[0].querySelector('.content-holder'));
        console.log('Loading');

        self.fillData = function () {
            $http.get($rootScope.baseUrl + 'app/client_insurances').then(function(response) {
                self.policies = response.data;
                self.noData = false;
                if(self.policies.length == 0){
                    self.noData = true;
                }
            });
        };

        self.initialization = function(){
            $http.get('base').then(function(response) {
                $rootScope.baseUrl = response.data;
                $http.get($rootScope.baseUrl + 'app/user').then(function(response) {
                    var data = response.data;
                    if (data.name) {
                        self.authenticated = true;
                        self.user = data.name
                        self.fillData();
                    } else {
                        self.authenticated = false;
                    }
                }, function() {
                    self.authenticated = false;
                });
            });
        };

        self.logout = function() {
            $http.post($rootScope.baseUrl + 'logout', {}).finally(function() {
                self.authenticated = false;
                window.location = $rootScope.baseUrl;
            });
        };

        self.initialization();

        self.newPolicyPopup = function(){
            var modalInstance = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/ui/modal-templates/new-insurance.html',
                controller: 'newInsurance',
                controllerAs: '$ctrl',
                appendTo: self.modalParent
            });

            modalInstance.result.then(function (response) {
                if(response.status == 200){
                    self.showSuccessMsg= true;
                    self.successMsg = "Your policy was created successfully.";
                    $timeout(function () {
                        self.showSuccessMsg= false;
                        self.successMsg = "";
                    }, 3000);
                }else{
                    self.showErrorMsg= true;
                    self.errorMsg = "An error occured while creating your policy.";
                    $timeout(function () {
                        self.showErrorMsg= false;
                        self.errorMsg = "";
                    }, 3000);
                }
                self.fillData();
            }, function () {
                $log.info('Policy creation canceled by user action.');
            });
        };

        self.cancelConfirm = function(policy){
            var modalInstance = $uibModal.open({
                animation: false,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/ui/modal-templates/delete-insurance-confirm.html',
                controller: 'deleteInsurance',
                controllerAs: '$ctrl',
                appendTo: self.modalParent,
                resolve: {
                    insuranceDTO: function () {
                        return policy;
                    }
                }
            });

            modalInstance.result.then(function (response) {
                if(response.status == 200){
                    self.showSuccessMsg= true;
                    self.successMsg = "Your policy was deleted successfully.";
                    $timeout(function () {
                        self.showSuccessMsg= false;
                        self.successMsg = "";
                    }, 3000);
                }else{
                    self.showErrorMsg= true;
                    self.errorMsg = "An error occured while deleting your policy.";
                    $timeout(function () {
                        self.showErrorMsg= false;
                        self.errorMsg = "";
                    }, 3000);
                }
                self.fillData();
            }, function () {
                $log.info('Policy deletion canceled by user action.');
            });
        };
    });