angular.module('clientapp').controller('deleteInsurance', function ($uibModalInstance, insuranceDTO, $http, $rootScope){
    var $ctrl = this;

    $ctrl.insuranceDTO = insuranceDTO;

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $ctrl.ok = function () {
        $ctrl.insuranceDTO.name = $ctrl.insuranceDTO.insuranceType.id;
        $http.post($rootScope.baseUrl + 'app/delete_insurance', $ctrl.insuranceDTO)
            .then(function(response) {
                $uibModalInstance.close(response);
        });
    };
});