angular.module('clientapp').controller('newInsurance', function ($uibModalInstance, $http, $rootScope, $timeout) {
    var $ctrl = this;

    $ctrl.insuranceDTO = {};

    $http.get($rootScope.baseUrl + 'app/insurance_types').then(function(response) {
        $ctrl.categories = response.data;
    });

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $ctrl.ok = function () {
        $http.post($rootScope.baseUrl + 'app/create_insurance', $ctrl.insuranceDTO)
            .then(function(response) {
                $uibModalInstance.close(response);
            });
    };

    $ctrl.changeCategory = function (category) {
        $ctrl.insuranceDTO.insuranceType = category;
        $ctrl.insuranceDTO.name = category.id; //For annotation parsing
        $ctrl.insuranceDTO.coverage = 0;
        $ctrl.coverageChange();
    };

    //we don't want it to send too many requests, so we wait
    //2 seconds after the last change to send the request
    $ctrl.tariffEnqure = undefined;
    $ctrl.coverageChange = function () {
        if($ctrl.insuranceDTO.coverage < $ctrl.insuranceDTO.insuranceType.minimumCoverage){
           $ctrl.insuranceDTO.coverage = $ctrl.insuranceDTO.insuranceType.minimumCoverage;
           if($ctrl.insuranceDTO.coverage <= 0){
               $ctrl.insuranceDTO.coverage =1;
           }
        }
        else if($ctrl.insuranceDTO.coverage > $ctrl.insuranceDTO.insuranceType.maximumCoverage){
            $ctrl.insuranceDTO.coverage = $ctrl.insuranceDTO.insuranceType.maximumCoverage;
        }
        $timeout.cancel($ctrl.tariffEnqure);
        $ctrl.tariffEnqure = $timeout(function () {
            $http.post($rootScope.baseUrl + 'app/calculate_coverage', $ctrl.insuranceDTO)
                .then(function(response) {
                    if( !isNaN(parseFloat(response.data)) && isFinite(response.data)){
                        $ctrl.insuranceDTO.tariff = response.data;
                    }
                });
        }, 2000);
    };
});