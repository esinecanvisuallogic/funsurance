package org.esinecan.funsurance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FunsuranceUIApplication {

	public static void main(String[] args) {
		SpringApplication.run(FunsuranceUIApplication.class, args);
	}
}
