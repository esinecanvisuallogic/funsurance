package org.esinecan.funsurance.ui.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by eren.sinecan
 */
@RestController
@PropertySource("/initialization.properties")
public class URLConfiguration {

    @Value("${funsurance.base.url}")
    private String baseUrl;

    @RequestMapping("/base")
    public String getBaseUrl() {
        return baseUrl;
    }

}
