package org.esinecan.funsurance.ui.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by eren.sinecan
 */
@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@PropertySource("/initialization.properties")
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${funsurance.role.user}")
    private String userRole;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
                .httpBasic().and()
                .authorizeRequests()
                .antMatchers("/", "/user", "/base", //Configuration endpoints for UI
                        "/index.html", "/modal-templates/**"
                        ,"/uib/**"
                ).permitAll()
                .anyRequest().hasRole("USER");
        // @formatter:on
    }
}

