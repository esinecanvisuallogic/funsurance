# Funsurance - An insurance app (sort of) #

This is a Spring Cloud project implemented following API Gateway Pattern. As storage, it uses Redis and H2 db. We use embedded h2, and I included redis as well. So you don't need to install anything extra to run the project. Just run *mvn package* in the root folder so that the wro4j produces static UI assets necessary. One key thing is to run the funsurance-auth project first. Other projects won't be reachable without authorizing there. The order is not important for the others. 

To test, starting with the funsurance-auth, just go into each project directory and run *mvn spring-boot:run*
 
I have used embedded Redis for ease of set up, but it's not very stable. So I'm also including a redis server in project root folder. If you have any problems with the embedded one, just go in the directory and enter this command: "src/redis-server" but I don't expect you'll have any problems.

I'm not including Spring Cloud Config Server so that starting up the project does not become a hassle for anyone.

Finally, the amount of unit and integration tests are way less than I'd like, but I had trouble allocating the time I'm afraid.

## Project Structure ##

### funsurance-entities ###

This is the only project you don't need to run *mvn spring-boot:run* on. This project just holds a bunch of repositories and entities that are used by both the auth, admin and the logic projects. It increases the coupling between them, but that's the most common way of achieving DRY when two projects work on same entities like this. So running a *mvn package* should be perfectly sufficient.

### funsurance-auth ###

This is the API Gateway project. So we needed a shared data source to keep the session once our users authenticate from here. It has an embedded Redis which holds the sessions. Because it's supported out of the box, and embeddable, it seemed like the easier option. Similarly, it has an embedded H2 database TCP server as well. Both servers are started when the application starts.

Aside from that, it holds the REST endpoints for login and registration. We do not let the user interact with other projects until the session is saved here. Also it has a single view tiny Angular app on it, so that we can provide the user with initial screens. This Angular app uses only wro4j, I did not have the time to write a proper Bower task for it. I might come back and refactor that. Another such TODO item is Jasmine tests. I'll probably add some later on.

The JPA entities on this project are Privilege (Only READ and WRITE at the moment), Role (Only USER and ADMIN at the moment) and User. We read/write these entities through their own simple JPA repositories. These repos also reside in this project. 

Through the use of three ConstraintValidators, we force the user to enter a valid e-mail address, a password that has no white space and has the size of 5<= password length <= 30, and a valid re-entry of that password to be sure.

Using a custom WebSecurityConfigurerAdapter configuration, we also produce CSRF tokens so as not to deal with Preflight CORS implementation.

Also this project has a tiny bit of bdd in the implementation of registration through the usage of cucumber and its features. If I implement any more features in the future, this can provide a guideline.

Finally, since this project is API Gatekeeper, it needs to act accordingly and calls to other services should be relayed by this service. That is achieved through Zuul calls. Routing of Zuul calls can be found at funsurance-auth > application.yml

This part of the project runs at localhost:8089

### funsurance-ui ###

This is the interface of the funsurance-logic project. Basically only serves static html and js files.
 
### funsurance-logic ###
 
This is a Restful microservice project that is accessed by funsurance-ui through calls relayed by our gatekeeper, funsurance-auth. Server side of the ui project.
Although it wasn't in the spec, I assumed that every insurance policy had some unique attributes that prevent us from putting them in the same type as a challenge.
So this is the layer I attempted to create a generic mechanism for varying kinds of insurance policies. I went with Enum based Strategy Pattern.

Now, to register a new InsuranceType, one would need to:
- insert a InsuranceType entity into db.
- Create the entity class with its own table
- Create the repository
- Create the strategy service
- Register in the strategy enum
- Create the DTO
- Register the DTO as JsonSubTypes.Type
- Adapt front end if the new type entails any specific actions