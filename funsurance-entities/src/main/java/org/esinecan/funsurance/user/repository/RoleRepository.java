package org.esinecan.funsurance.user.repository;

import org.esinecan.funsurance.user.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by eren.sinecan
 */
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);

    @Override
    void delete(Role role);

}
