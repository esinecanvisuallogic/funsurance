package org.esinecan.funsurance.user.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

/**
 * This is the privilege entity that belongs to roles. Although we do not exercise fine grained authorization
 * with many different privileges such as auditor, productReader, etc. this is the convention.
 * Created by eren.sinecan
 */
@Entity
@Table(name = "privilege")
@Setter
@Getter
public class Privilege {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "privileges")
    private Collection<Role> roles;
}