package org.esinecan.funsurance.user.service.contract;

/**
 * Created by eren.sinecan
 */
public interface UserRoleService {
    String getUserRolesCSV (String username);
}
