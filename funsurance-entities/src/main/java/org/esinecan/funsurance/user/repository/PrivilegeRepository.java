package org.esinecan.funsurance.user.repository;

import org.esinecan.funsurance.user.model.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by eren.sinecan
 */
public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {

    Privilege findByName(String name);

    @Override
    void delete(Privilege privilege);

}
