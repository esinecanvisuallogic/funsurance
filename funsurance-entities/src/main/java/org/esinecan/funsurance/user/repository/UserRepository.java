package org.esinecan.funsurance.user.repository;

import org.esinecan.funsurance.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by eren.sinecan
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}

