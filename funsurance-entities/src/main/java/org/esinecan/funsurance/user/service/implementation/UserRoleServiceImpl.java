package org.esinecan.funsurance.user.service.implementation;

import org.esinecan.funsurance.user.model.Role;
import org.esinecan.funsurance.user.model.User;
import org.esinecan.funsurance.user.repository.UserRepository;
import org.esinecan.funsurance.user.service.contract.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by eren.sinecan
 */
@Service
public class UserRoleServiceImpl implements UserRoleService{

    @Autowired
    private UserRepository userRepository;

    /**
     * Retrieves up to date roles of users.
     * @param username
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public String getUserRolesCSV(String username) {
        User user = userRepository.findByUsername(username);
        Collection<Role> roles = user.getRoles();
        return roles.stream().map(role -> role.getName()).collect(Collectors.joining(","));
    }
}
