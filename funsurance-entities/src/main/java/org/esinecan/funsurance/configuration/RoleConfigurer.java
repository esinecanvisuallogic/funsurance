package org.esinecan.funsurance.configuration;

import org.esinecan.funsurance.user.model.Privilege;
import org.esinecan.funsurance.user.model.Role;
import org.esinecan.funsurance.user.repository.PrivilegeRepository;
import org.esinecan.funsurance.user.repository.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Since we're using h2 db, our database resets every time we restart our application.
 * This service is called to insert necessary user roles. Could also have been done with
 * a sql file but I favor annotated classes over other configuration options.
 * Created by eren.sinecan
 */
@Component
public class RoleConfigurer {

    private static final Logger logger = LoggerFactory.getLogger(RoleConfigurer.class);

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Transactional
    public boolean configureRolesAndPrivileges(String adminRole, String userRole,
                                               String readPrivilege, String writePrivilege){
        Privilege read
                = createPrivilegeIfNotFound(readPrivilege);
        Privilege write
                = createPrivilegeIfNotFound(writePrivilege);

        List<Privilege> adminPrivileges = Arrays.asList(
                read, write);
        createRoleIfNotFound("ROLE_" + adminRole, adminPrivileges);
        createRoleIfNotFound("ROLE_" + userRole, Arrays.asList(read));
        return true;
    }

    private Privilege createPrivilegeIfNotFound(String name) {

        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege();
            privilege.setName(name);
            privilegeRepository.save(privilege);
            logger.debug("Created privilege: " + privilege.getName());
        }
        return privilege;
    }

    private Role createRoleIfNotFound(
            String name, Collection<Privilege> privileges) {

        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role();
            role.setName(name);
            role.setPrivileges(privileges);
            roleRepository.save(role);
            logger.debug("Created role: " + role.getName());
        }
        return role;
    }
}
