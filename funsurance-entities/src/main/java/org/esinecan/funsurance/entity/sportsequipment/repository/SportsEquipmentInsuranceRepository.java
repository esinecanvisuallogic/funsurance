package org.esinecan.funsurance.entity.sportsequipment.repository;


import org.esinecan.funsurance.entity.sportsequipment.model.SportsEquipmentInsurance;
import org.esinecan.funsurance.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * An insurance plan entity. Has its own table.
 * Unlike other entities of same category, this one, (assuming it's made of gold) has a carat consumption field.
 * Created by eren.sinecan
 */
@Repository
public interface SportsEquipmentInsuranceRepository extends JpaRepository<SportsEquipmentInsurance, Long> {
    List<SportsEquipmentInsurance> findByUser(User user);
}
