package org.esinecan.funsurance.entity.archetype.repository;


import org.esinecan.funsurance.entity.archetype.model.InsuranceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository class for our insurance types. As of now, we have only 4.
 * Created by eren.sinecan
 */
@Repository
public interface InsuranceTypeRepository extends JpaRepository<InsuranceType, String>{
    List<InsuranceType> findAll();

    InsuranceType findById(String id);

    InsuranceType save(InsuranceType insuranceType);
}
