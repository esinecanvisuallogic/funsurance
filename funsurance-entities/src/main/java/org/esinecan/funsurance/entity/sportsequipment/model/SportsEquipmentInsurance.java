package org.esinecan.funsurance.entity.sportsequipment.model;

import lombok.Getter;
import lombok.Setter;
import org.esinecan.funsurance.entity.base.model.BaseInsurance;
import org.esinecan.funsurance.entity.base.model.IBaseInsurance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * An insurance plan entity. Has its own table.
 * Unlike other entities of same category, this one, (assuming it's very old) has an antiquityNotes consumption field.
 * Created by eren.sinecan
 */
@Entity
@Table(name = "sports_insurances")
@Getter
@Setter
public class SportsEquipmentInsurance extends BaseInsurance implements IBaseInsurance{

    public SportsEquipmentInsurance(){
        super();
    }
    /**
     * Let's give every type of insurance it's own unique requirements.
     */
    @Column
    private String antiquityNotes;
}
