package org.esinecan.funsurance.entity.bike.repository;


import org.esinecan.funsurance.entity.bike.model.BikeInsurance;
import org.esinecan.funsurance.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for Bike insurance plan.
 * Created by eren.sinecan
 */
@Repository
public interface BikeInsuranceRepository extends JpaRepository<BikeInsurance, Long> {
    List<BikeInsurance> findByUser(User user);
}
