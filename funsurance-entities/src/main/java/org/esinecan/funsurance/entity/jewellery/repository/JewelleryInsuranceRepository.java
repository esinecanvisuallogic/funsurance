package org.esinecan.funsurance.entity.jewellery.repository;


import org.esinecan.funsurance.entity.jewellery.model.JewelleryInsurance;
import org.esinecan.funsurance.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for Jewellery insurance plan.
 * Created by eren.sinecan
 */
@Repository
public interface JewelleryInsuranceRepository extends JpaRepository<JewelleryInsurance, Long> {
    List<JewelleryInsurance> findByUser(User user);
}
