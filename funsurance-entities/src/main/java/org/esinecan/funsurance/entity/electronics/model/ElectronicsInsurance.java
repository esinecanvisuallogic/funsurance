package org.esinecan.funsurance.entity.electronics.model;

import lombok.Getter;
import lombok.Setter;
import org.esinecan.funsurance.entity.base.model.BaseInsurance;
import org.esinecan.funsurance.entity.base.model.IBaseInsurance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * An insurance plan entity. Has its own table.
 * Unlike other entities of same category, this one has a hourly power consumption field.
 * Created by eren.sinecan
 */
@Entity
@Table(name = "electronic_insurances")
@Getter
@Setter
public class ElectronicsInsurance extends BaseInsurance implements IBaseInsurance{

    public ElectronicsInsurance(){
        super();
    }
    /**
     * Let's give every type of insurance it's own unique requirements.
     */
    @Column
    private Double kwPerHour;
}
