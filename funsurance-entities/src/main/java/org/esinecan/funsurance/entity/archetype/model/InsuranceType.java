package org.esinecan.funsurance.entity.archetype.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Type data of insurance plan object. Every insurance plan has to have one of these
 * for us to be able to validate user submitted tariff and coverage values.
 * Created by eren.sinecan
 */
@Entity
@Table(name = "insurance_types")
@NoArgsConstructor
@Getter
@Setter
public class InsuranceType {

    @Id
    private String id;

    @Column
    private Double riskPercentage;

    @Column
    private Double minimumCoverage;

    @Column
    private Double maximumCoverage;

    @Column
    private String name;
}
