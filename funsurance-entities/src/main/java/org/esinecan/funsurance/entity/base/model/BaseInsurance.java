package org.esinecan.funsurance.entity.base.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.esinecan.funsurance.entity.archetype.model.InsuranceType;
import org.esinecan.funsurance.user.model.User;

import javax.persistence.*;

/**
 * This is the base class that holds shared properties (almost all of them) of our insurance plans.
 * From a OOP standpoint, This could have been part of InsuranceType but this implementation makes more
 * sense from DDD perspective.
 * Created by eren.sinecan
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@NoArgsConstructor
public abstract class BaseInsurance implements IBaseInsurance{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    protected Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "insuranceType")
    protected InsuranceType insuranceType;

    @Column
    private Double coverage;


    @Column
    private Double tariff;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user")
    @JsonIgnore
    protected User user;

}