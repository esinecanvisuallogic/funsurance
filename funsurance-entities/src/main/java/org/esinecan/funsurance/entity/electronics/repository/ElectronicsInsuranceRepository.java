package org.esinecan.funsurance.entity.electronics.repository;


import org.esinecan.funsurance.entity.electronics.model.ElectronicsInsurance;
import org.esinecan.funsurance.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for Electronics insurance plan.
 * Created by eren.sinecan
 */
@Repository
public interface ElectronicsInsuranceRepository extends JpaRepository<ElectronicsInsurance, Long> {
    List<ElectronicsInsurance> findByUser(User user);
}
