package org.esinecan.funsurance.entity.jewellery.model;

import lombok.Getter;
import lombok.Setter;
import org.esinecan.funsurance.entity.base.model.BaseInsurance;
import org.esinecan.funsurance.entity.base.model.IBaseInsurance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * An insurance plan entity. Has its own table.
 * Unlike other entities of same category, this one, (assuming it's made of gold) has a carat consumption field.
 * Created by eren.sinecan
 */
@Entity
@Table(name = "jewellery_insurances")
@Getter
@Setter
public class JewelleryInsurance extends BaseInsurance implements IBaseInsurance{

    public JewelleryInsurance(){
        super();
    }
    /**
     * Let's give every type of insurance it's own unique requirements.
     */
    @Column
    private String carat;
}
